/**Реализовать функцию полного клонирования объекта.

 Технические требования:

 Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке,
 внутренняя вложенность свойств объекта может быть достаточно большой).
 Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
 В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign()
 или спред-оператор.*/

const person = {
    name: 'Ivan',
    age: 24,
    position: 'developer',
    skills: ['HTML', 'CSS', 'JS', 'Sass', 'Scss']
};

function cloneObject(obj) {
    const clonedObj = {};
    for (let key in obj) {
        if (typeof key === 'object') {
            cloneObject();
        } else {
            clonedObj[key] = obj[key]
        }
    }
    console.log(clonedObj);
}

cloneObject(person);